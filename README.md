# sisop-praktikum-modul-2-2023-BJ-A08

Kelompok A08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Robby Ulung Pambudi | 5025211042 |
| Mohammad Zhafran Dzaky | 5025211142 |
| Ihsan Widagdo | 5025211231 |

## Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

### Point A
#### Deskripsi 
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

#### Penyelesaian
Untuk mendownload file tersebut kita membutuhkan tool wget, untuk memasang tool wget kita bisa menggunakan perintah berikut :
```
sudo apt-get install wget
```

Kemudian untuk mendownload file tersebut kita bisa menggunakan perintah berikut :
```c
char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
execv("/usr/bin/wget", args);
```

#### Code
```c
void download(char *url, char *namefile)
{
  int stat;
  pid_t downloadID = fork();
  if (downloadID == 0)
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
    execv("/usr/bin/wget", args);
  }
  waitpid(downloadID, &stat, 0);
}
```
#### Penjelasan
1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
2. `pid_t downloadID = fork();` : untuk mendeklarasikan variabel downloadID yang bertipe data `pid_t` dan menginisialisasikan variabel tersebut dengan fungsi `fork()`
3. `if (downloadID == 0)` : untuk melakukan pengecekan apakah variabel downloadID bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
4. `execv("/usr/bin/wget", args);` : untuk menjalankan program wget dengan argumen args
5. `waitpid(downloadID, &stat, 0);` : untuk menunggu proses child selesai

### Point B
#### Deskripsi
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

#### Penyelesaian
Untuk melakukan unzip file tersebut kita membutuhkan tool unzip, untuk memasang tool unzip kita bisa menggunakan perintah berikut :
```
sudo apt-get install unzip
```
Untuk melakukan unzip file tersebut kita bisa menggunakan perintah berikut :
```c
char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
```
### Code
```c
void unzip(char *sourceDir)
{
  int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }

  waitpid(unzipID, &stat, 0);
}
```
#### Penjelasan
1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
2. `pid_t unzipID = fork();` : untuk mendeklarasikan variabel unzipID yang bertipe data `pid_t` dan menginisialisasikan variabel tersebut dengan fungsi fork()`
3. `unzipID == 0` : untuk melakukan pengecekan apakah variabel unzipID bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses `child`
4. `execv("/usr/bin/unzip", args);` : untuk menjalankan program unzip dengan argumen args
5. `waitpid(unzipID, &stat, 0)`; : untuk menunggu proses child selesai

>**Note** : Kenapa kita mengunakan variable `stat`? merupakan pointer ke variabel yang menyimpan status yang dikembalikan oleh child process. 

### Point C
#### Deskripsi  
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

#### Penyelesaian
Untuk membuat direktori kita bisa menggunakan perintah berikut :
```c
char *args[] = {"mkdir", "-p", dir, NULL};
execv("/bin/mkdir", args);
```
Untuk melakukan pemindahan file kita bisa menggunakan perintah berikut :
```c
char *args[] = {"mv", sourceDir, destDir, NULL};
execv("/bin/mv", args);
```

Pada persoalan ini kita menggunakan 2 fungsi yaitu fungsi untuk membuat direktori dan fungsi untuk memindahkan file.

#### Code
- Fungsi untuk membuat direktori
```c
void generateDirectory()
{
  int stat;
  id_t child_id;

  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanDarat", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;

  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAir", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAmphibi", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;

  waitpid(child_id, &stat, 0);
}
```
- Fungsi untuk memindahkan file
```c
void moveAnimal(char *soruce, char *des1, char *des2, char *des3)
{
  int stat;
  id_t child_id;
  struct dirent *dp;
  DIR *folder;
  folder = opendir(soruce);

  if (folder != NULL)
  {
    while ((dp = readdir(folder)) != NULL)
    {
      if (dp->d_type == DT_REG)
      {
        // if match with darat
        if (strstr(dp->d_name, "darat") != NULL)
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des1, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "air") != NULL)
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des2, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "amphibi") != NULL)
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des3, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
      }
    }
    closedir(folder);
  }
  waitpid(child_id, &stat, 0);
}
```
#### Penjelasan
- Fungsi untuk membuat direktori
  1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
  2. `id_t child_id;` : untuk mendeklarasikan variabel `child_id` yang bertipe data `id_t`
  3. `if ((child_id = fork()) == 0`) : untuk melakukan pengecekan apakah variabel child_id bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
  4. `while ((wait(&stat)) > 0)` : untuk menunggu proses child selesai
  5. Melakukan pemanggilan fungsi `execv()` untuk menjalankan program mkdir dengan argumen args
  6. Menjalankan proses `3 - 5` sebanyak 3 kali untuk membuat 3 direktori yaitu HewanDarat, HewanAir, dan HewanAmphibi

- Fungsi untuk memindahkan direktori
  1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
  2. `id_t child_id;` : untuk mendeklarasikan variabel `child_id` yang bertipe data `id_t`
  3. `struct dirent *dp;` : untuk mendeklarasikan variabel `dp` yang bertipe data `struct dirent`
  4. `DIR *folder;` : untuk mendeklarasikan variabel `folder` yang bertipe data `DIR`
  5. `folder = opendir(soruce);` : untuk membuka direktori yang akan dipindahkan
  6. `if (folder != NULL)` : untuk melakukan pengecekan apakah direktori yang akan dipindahkan ada atau tidak
  7. `while ((dp = readdir(folder)) != NULL)` : untuk melakukan perulangan selama direktori yang akan dipindahkan masih ada
  8. `if (dp->d_type == DT_REG)` : untuk melakukan pengecekan apakah file yang akan dipindahkan merupakan file biasa atau bukan
  9. `if (strstr(dp->d_name, "darat") != NULL)` : untuk melakukan pengecekan apakah file yang akan dipindahkan merupakan file yang memiliki nama yang mengandung kata darat
  10. `if ((child_id = fork()) == 0)` : untuk melakukan pengecekan apakah variabel child_id bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
  11. `char *argv[] = {"mv", dp->d_name, des1, NULL};` : untuk mendeklarasikan variabel `argv` yang bertipe data `char` dan menginisialisasikan variabel tersebut dengan argumen yang akan digunakan untuk menjalankan program mv
  12. `execv("/bin/mv", argv);` : untuk menjalankan program mv dengan argumen args
  13. `while ((wait(&stat)) > 0)` : untuk menunggu proses child selesai
  14. `if (strstr(dp->d_name, "air") != NULL)` : untuk melakukan pengecekan apakah file yang akan dipindahkan merupakan file yang
  15. `if (strstr(dp->d_name, "amphibi") != NULL)` : untuk melakukan pengecekan apakah file yang akan dipindahkan merupakan file yang memiliki nama yang mengandung kata amphibi
#### Output
[Contoh-Output-Point-C](https://asset.cloudinary.com/dqpzaw0pm/4443b00ffc30b78f7008c5adb1e8c2a1)

### Point D
#### Deskripsi
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

#### Penyelesaian
Untuk menyelesaikan point D, memerlukan fungsi zip,
untuk menjalankan fungsi zip pada `c` seperti berikut 
```c
char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
execv("/usr/bin/zip", argv);
```

#### Code 
```c
void zipAnimal()
{
  id_t child_id;
  int stat;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  waitpid(child_id, &stat, 0);
}
```
#### Penjelasan
1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
2. ` id_t child_id;` : untuk mendeklarasikan variabel `child_id` yang bertipe data `id_t`
3. ` if ((child_id = fork()) == 0)` : untuk melakukan pengecekan apakah variabel `child_id` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
4. `char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL}; `: untuk mendeklarasikan variabel argv yang bertipe data char dan menginisialisasikan variabel tersebut dengan argumen yang akan digunakan untuk menjalankan program zip
5. Begitu juga untuk folder HewanAir dan HewanAmphibi
6. `while ((wait(&stat)) > 0)` : untuk menunggu proses child selesai
7. `waitpid(child_id, &stat, 0);` : untuk menunggu proses child selesai

#### Output
[Point D](https://asset.cloudinary.com/dqpzaw0pm/7203f3290200a30e5cdbd5992092616a)

## Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

### Point A
#### Deskripsi
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

#### Penyelesaian
untuk menyelesaikan point A, kita perlu menjalankan perintah `mkdir` dengan argumen dan mengambil info waktu dengan fungsi `time()` dengan cara sebagai berikut

```c
time(&waktu);
info = localtime(&waktu);
strftime(usrtime, 100, "%Y-%m-%d_%X", info);
sprintf(path, "%s/%s", dir, usrtime);
```
untuk mendapatkan waktu sekarang dan 
```c
char *argfolder[] = {"mkdir", "-p", path, NULL};
execv("/bin/mkdir", argfolder);
```
untuk melakukan pembuatan folder dengan nama timestamp yang telah kita dapatkan.

#### Code
- Untuk mendapatkan waktu
```c
time_t waktu;
char usrtime[100];
char path[100];
struct tm *info;
time(&waktu);
info = localtime(&waktu);
strftime(usrtime, 100, "%Y-%m-%d_%X", info);
sprintf(path, "%s/%s", dir, usrtime);
```
- Untuk melakukan pembuatan folder dengan nama timestamp
```c
parent = fork();
if(parent < 0)
  exit(EXIT_FAILURE);
if(parent == 0){
    pid_t donlod;
    int status_donlod;
    donlod = fork();
    if(donlod < 0)
      exit(EXIT_FAILURE);
    if(donlod == 0){
      pid_t folder;
      folder = fork();
      if(folder < 0)
        exit(EXIT_FAILURE);
      //child proccess untuk melakukan mkdir
      if(folder == 0){
        char *argfolder[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argfolder);
      }
    }
}
```

#### Penjelasan
1. `time_t waktu` mendeklarasikan variabel waktu dengan tipe data time_t
2. `struct tm *info` membuat struct waktu dengan variabel info
3. `time()` fungsi untuk mendapatkan waktu
4. `fork()` fungsi untuk menciptakan parent process dan child process
5. `char *argfolder[] = {"mkdir", "-p", path, NULL}` argumen untuk menjalankan perintah mkdir
6. `execv("/bin/mkdir", argfolder)` fungsi untuk menjalankan perintah mkdir

#### Output
- [Output 1](https://drive.google.com/file/d/19-68WrR60LD8RpsaAKeReSMLA1a8F2II/view?usp=sharing)

### Point B
#### Deskripsi
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

#### Penyelesaian
Untuk menyelesaikan point B, kita perlu membuat variabel untuk menyimpan rumus perhitungan yang telah ditentukan untuk mendapat ukuran dari gambarnya, lalu menjalankan perintah wget dari url pada soal dengan perintah sebagai berikut :
```c
char *argv[] = {"wget", "-q", "-O", dir_download, url, NULL};
execv("/usr/bin/wget", argv);
```
dan kita sleep tiap 5 detik agar gambar diunduh tiap 5 detik
#### Code
- Untuk melakukan unduh gambar sesuai yang diminta soal
```c
ull waktu = (ull)time(NULL);
waktu = (waktu%1000) + 50;
char url[100];
sprintf(url, "https://picsum.photos/%llu", waktu);
time_t time_download;
struct tm *downloadt;
char name_time[100];
char dir_download[100];

for (int i = 0; i < 15; i++){
  time(&time_download);
  downloadt = localtime(&time_download);
  strftime(name_time, 100, "%Y-%m-%d_%X", downloadt);
  sprintf(dir_download, "%s/%s", usrtime, name_time);
  pid_t gambar = fork();
  if(gambar < 0)
    exit(EXIT_FAILURE);
  if(gambar == 0){
  //proccess dari child gambar untuk melakukan download gambar
    char *argv[] = {"wget", "-q", "-O", dir_download, url, NULL};
    execv("/usr/bin/wget", argv);
  }
  sleep(5);
}
```
#### Penjelasan
1. `char *argv[] = {"wget", "-q", "-O", dir_download, url, NULL}` argumen untuk melakukan perintah wget
2. `execv("/usr/bin/wget", argv)` fungsi untuk melakukan perintah wget
3. `sleep(5)` fungsi untuk memberi jeda selama 5 detik di tiap pengunduhan gambar

#### Output
- [Output 2](https://drive.google.com/file/d/1WW-UANCQYDCUgr1LzRw7xdUMXbSi2xw0/view?usp=sharing)

### Point C
#### Deskripsi
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

#### Penyelesaian
Untuk menyelesaikan point C, memerlukan fungsi `wait()` untuk menunggu hingga unduh gambar mencapai 15 dan perintah untuk melakukan zip directory serta meremove directory yang telah kita zip dengan perintah sebagai berikut :
```c
char *argv[] = {"zip", "-rm", zip_name, path, NULL};
execv("/usr/bin/zip", argv);
```

#### Code
```c
wait(&status_donlod);
void ngezip(char *path, char *usrtime){
    char zip_name[100];
    sprintf(zip_name, "%s.zip", usrtime);
    char *argv[] = {"zip", "-rm", zip_name, path, NULL};
    execv("/usr/bin/zip", argv);
}
```

#### Penjelasan
1. `wait()` fungsi untuk menunggu hingga process berjalan hingga selesai
2. `char *argv[] = {"zip", "-rm", zip_name, path, NULL}` argumen untuk melakukan zip direktori serta remove direktori
3. `execv("/usr/bin/zip", argv)` fungsi untuk menjalankan perintah zip

#### Output
- [Output 3](https://drive.google.com/file/d/1muX_ke_f1EtJgrh6wUMo1LnJEivSXdvL/view?usp=sharing)

### Point D dan E
#### Deskripsi
- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

#### Penyelesaian
Untuk menyelesaikan point D dan E diperlukan fungsi untuk membuat killer dengan mode yang berbeda. Pertama kita perlu passingkan argumen pada fungsi killer agar kita tahu killer mode apa yang akan dibuat oleh program, lalu jika argumen yang dimasukkan merupakan  `-a` maka akan dibuat killer sebagai berikut:
```c
strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);")
```
namun jika argumen yang dimasukkan adalah `-b` maka akan dibuat killer sebagai berikut:
```c
sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid)
```
Lalu juga diperlukan perintah untuk mengcompile killer dan meremove si killer ketika killer dijalankan, hal tersebut dilakukan dengan cara berikut:
```c
char *argv[] = {"gcc", file_name, "-o", dest, NULL};
execv("/usr/bin/gcc", argv);
```
```c
char *argv[] = {"rm", src, NULL};
execv("/bin/rm", argv);
```

#### Code
- Fungsi untuk membuat killer serta mengcompile dan meremove killer
```c
void killer(pid_t pid, char param){
    char cwd[100];
    //mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;
    int test;
    //melakukan forking pada parent
    pid_t parent = fork();
    if (parent < 0) 
        exit(EXIT_FAILURE);

    if (parent == 0) {
        FILE *fp;
        char killer_file[100];
        char data[1000];
        char dest[100];
        char file_name[100];
        char mode[100];
        
        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);

        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a') 
          strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b') 
          sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status)>0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    while(wait(&status) > 0);
    //melakukan forking pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0) 
      exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[100];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }
    
    while(wait(&status) > 0);
}
```

#### Penjelasan
1. `strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv)` argumen serta fungsi untuk menjalankan perintah killall untuk mematikan semua process
2. `sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid)` argumen serta fungsi untuk menjalankan perintah killall untuk mematikan parent process
3. `char *argv[] = {"gcc", file_name, "-o", dest, NULL}` argumen untuk mengcompile file killer
4. `execv("/usr/bin/gcc", argv)` fungsi untuk menjalankan perintah compile program c
5. `while(wait())` fungsi untuk menunggu hingga process selesai dilakukan
6. `char *argv[] = {"rm", src, NULL}` argumen untuk meremove file killer
7. `execv("/bin/rm", argv)` fungsi untuk menjalankan perintah remove
8. `getcwd(cwd, sizeof(cwd))` fungsi untuk mendapatkan path direktori kita sekarang

#### Output
- [Output 4](https://drive.google.com/file/d/1w-tVJ9hXxO0IIdvwC0Xt_91uKTqkcVJ6/view?usp=sharing)
- [Output 5](https://drive.google.com/file/d/10VeQhIiQ3fv3l_M3kol2aJ1x3Iu2SuC9/view?usp=sharing)
- [Output 6](https://drive.google.com/file/d/1VSiHSHC_C2qAo97H53cL321Syis_pl1u/view?usp=sharing)

### Kendala
- Lumayan kesulitan untuk mencari cara agar bisa compile file killer dari program c di dalam program c

## Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

### Point A
#### Deskripsi
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

#### Penyelesaian
Untuk menyelesaikan point A, memerlukan fungsi download, extract, dan remove.
Untuk melakukan download pada `c` maka perlu menggunakan fungsi `wget` seperti berikut
```c
char *argv[] = {"wget", "--no-check-certificate", "-q", url, "-O", "players.zip", NULL};
execv("/usr/bin/wget", argv);
```
Untuk melakukan extract pada `c` maka perlu menggunakan fungsi `unzip` seperti berikut
```c
char *argv[] = {"unzip", "-q", "players.zip", NULL};
execv("/usr/bin/unzip", argv);
```
Untuk melakukan remove pada `c` maka perlu menggunakan fungsi `rm` seperti berikut
```c
char *argv[] = {"rm", "-f", "players.zip", NULL};
execv("/usr/bin/rm", argv);
```

#### Code
- Fungsi untuk download
```c
void donwload(char *url, char *namefile)
{
  int stat;
  pid_t downloadID = fork();

  if (downloadID == 0)
  {
    char *args[] = {"wget", "--no-check-certificate","-q", url, "-O", namefile, NULL};
    // ! Note: /urs/local/bin/wget is the path to wget in MacOS change it to /usr/bin/wget if you are using Linux
    execv("/usr/bin/wget", args);
  }
  waitpid(downloadID, &stat, 0);
}
```
- Fungsi untuk extract dan remove
```c
void unzip(char *sourceDir)
{
  int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
  while (wait(&stat) > 0)
    ;

  unzipID = fork();
  if (unzipID == 0)
  {
    char *args[] = {"rm", "-f", sourceDir, NULL};
    execv("/bin/rm", args);
  }
  while (wait(&stat) > 0)
    ;

  waitpid(unzipID, &stat, 0);
}
```
#### Penjelasan
- Fungsi untuk download
  1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
  2. `pid_t downloadID = fork();` : untuk mendeklarasikan variabel `downloadID` yang bertipe data `pid_t`
  3. `char *args[] = {"wget", "--no-check-certificate","-q", url, "-O", namefile, NULL};` : untuk mendeklarasikan variabel args yang bertipe data char dan menginisialisasikan variabel tersebut dengan argumen yang akan digunakan untuk menjalankan program wget
  4. `execv("/usr/bin/wget", args);` : untuk menjalankan program wget
  5. `waitpid(downloadID, &stat, 0);` : untuk menunggu proses child selesai

- Fungsi untuk extract dan remove
  1. `int stat;` : untuk mendeklarasikan variabel stat yang bertipe data integer
  2. `pid_t unzipID = fork();` : untuk mendeklarasikan variabel `unzipID` yang bertipe data `pid_t`
  3. `if (unzipID == 0)` : untuk melakukan pengecekan apakah variabel `unzipID` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
  4. `while (wait(&stat) > 0)` : untuk menunggu proses child selesai
  5. `unzipID = fork();` : untuk melakukan fork pada proses child yang sebelumnya telah selesai dijalankan
  6. `if (unzipID == 0)` : untuk melakukan pengecekan apakah variabel `unzipID` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
  7. `char *args[] = {"rm", "-f", sourceDir, NULL};` : untuk mendeklarasikan variabel args yang bertipe data char dan menginisialisasikan variabel tersebut dengan argumen yang akan digunakan untuk menjalankan program rm
  8. execv("/bin/rm", args);` : untuk menjalankan program rm
  9. `while (wait(&stat) > 0)` : untuk menunggu proses child selesai
  10. `waitpid(unzipID, &stat, 0);` : untuk menunggu proses child selesai

#### Output
[Point-3](https://asset.cloudinary.com/dqpzaw0pm/290bce4a46d8b62230cc4f291a427f72)


### Point B
#### Deskripsi
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.

#### Penyelesaian
untuk melakukan penyelesaian point B, perlu membuat fungsi `filter` yang akan melakukan pengecekan apakah nama pemain tersebut merupakan pemain Manchester United atau bukan. Jika bukan maka akan dihapus. Untuk melakukan pengecekan tersebut, perlu melakukan pembacaan folder `players` menggunakan fungsi `readdir` dan melakukan pengecekan apakah nama pemain tersebut merupakan pemain Manchester United atau bukan dengan menggunakan fungsi `strstr`. Jika nama pemain tersebut bukan pemain Manchester United, maka akan dihapus dengan menggunakan fungsi `rm`.

#### Code
```c
void removeNotMU(char *directory)
{
  int stat;
  char *path;
  id_t child_id;
  struct dirent *dp;
  DIR *folder;
  folder = opendir(directory);

  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
      {
        if (dp->d_type == DT_REG)
        {
          if (strstr(dp->d_name, "ManUtd") == NULL)
          {
            path = malloc(strlen(directory) + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", directory, dp->d_name);
            child_id = fork();
            if (child_id == 0)
            {
              char *args[] = {"rm", "-f", path, NULL};
              execv("/bin/rm", args);
            }
          }
        }
      }
    }
  }
  closedir(folder);
  waitpid(child_id, &stat, 0);
}
```
#### Penjelasan
- `char *path;` : untuk mendeklarasikan variabel `path` yang bertipe data char
- `id_t child_id;` : untuk mendeklarasikan variabel `child_id` yang bertipe data `id_t`
- `struct dirent *dp;` : untuk mendeklarasikan variabel `dp` yang bertipe data `struct dirent`
- `DIR *folder;` : untuk mendeklarasikan variabel `folder` yang bertipe data `DIR`
- `folder = opendir(directory);` : untuk membuka folder `players`
- `if (folder != NULL)` : untuk melakukan pengecekan apakah folder `players` berhasil dibuka atau tidak
- `int i = 0;` : untuk mendeklarasikan variabel `i` yang bertipe data integer dan menginisialisasikannya dengan nilai 0 untuk digunakan sebagai counter
- `while ((dp = readdir(folder)) != NULL)` : untuk melakukan pembacaan folder `players` menggunakan fungsi `readdir`
- `if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)` : untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki ekstensi `.png`
- `if (dp->d_type == DT_REG)` : untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki tipe `DT_REG`, `DT_REG` merupakan tipe file yang biasanya merupakan file biasa
- `if (strstr(dp->d_name, "ManUtd") == NULL)` : untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki nama pemain Manchester United atau bukan
- `path = malloc(strlen(directory) + strlen(dp->d_name) + 2);` : untuk melakukan alokasi memori untuk variabel `path` dengan ukuran memori yang dibutuhkan sesuai dengan panjang dari nama folder `players` dan nama file yang dibaca
- `sprintf(path, "%s/%s", directory, dp->d_name);` : untuk mengisi variabel `path` dengan nama folder `players` dan nama file yang dibaca
- `child_id = fork();` : untuk melakukan fork pada proses parent
- `if (child_id == 0)` : untuk melakukan pengecekan apakah variabel `child_id` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
- `closedir(folder);` : untuk menutup folder `players`
- `waitpid(child_id, &stat, 0);` : untuk menunggu proses child selesai


### Point C
#### Deskripsi
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

#### Penyelesaian
Untuk melakukan penyelesaian point C, perlu membuat fungsi `moveByRole` yang akan melakukan pembacaan folder `players` menggunakan fungsi `readdir` dan melakukan pengecekan apakah nama pemain tersebut merupakan pemain Manchester United atau bukan dengan menggunakan fungsi `strstr`. Jika nama pemain tersebut pemain Manchester United, maka akan dipindahkan ke folder sesuai dengan posisinya dengan menggunakan fungsi `mv`.

#### Code
```c
void moveByRole()
{
  int stat;
  char *path;
  pid_t child_id[4];
  struct dirent *dp;
  DIR *folder;

  for (int i = 0; i < 4; i++)
  {
    char *newFolder;
    switch (i)
    {
    case 0:
      newFolder = "Bek";
      break;
    case 1:
      newFolder = "Gelandang";
      break;
    case 2:
      newFolder = "Kiper";
      break;
    case 3:
      newFolder = "Penyerang";
      break;
    }
    child_id[i] = fork();
    if (child_id[i] < 0)
    {
      printf("Gagal membuat child process.\n");
      exit(EXIT_FAILURE);
    }
    else if (child_id[i] == 0)
    {
      char *args[] = {"mkdir", "-p", newFolder, NULL};
      execv("/bin/mkdir", args);
    }

    while (wait(&stat) > 0)
      ;

    if (folder != NULL)
    {
      folder = opendir("players");
      while ((dp = readdir(folder)) != NULL)
      {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
        {
          if (dp->d_type == DT_REG && strstr(dp->d_name, newFolder) != NULL)
          {
            path = malloc(strlen("players") + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", "players", dp->d_name);
            child_id[i] = fork();
            if (child_id[i] == 0)
            {
              char *args[] = {"mv", "-f", path, newFolder, NULL};
              execv("/bin/mv", args);
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < 4; i++)
  {
    waitpid(child_id[i], &stat, 0);
  }
  closedir(folder);
}
```
#### Penjelasan
- `char *path;` : untuk mendeklarasikan variabel `path` yang bertipe data char
- `int stat;` : untuk mendeklarasikan variabel `stat` yang bertipe data integer
- `pid_t child_id[4];` : untuk mendeklarasikan variabel `child_id` yang bertipe data `pid_t` dan memiliki ukuran array 4
- `struct dirent *dp;` : untuk mendeklarasikan variabel `dp` yang bertipe data `struct dirent`
- `DIR *folder;` : untuk mendeklarasikan variabel `folder` yang bertipe data `DIR`
- `for (int i = 0; i < 4; i++)` : untuk melakukan iterasi sebanyak 4 kali
- `char *newFolder;` : untuk mendeklarasikan variabel `newFolder` yang bertipe data char
- `swtich (i)` : untuk melakukan pengecekan apakah variabel `i` bernilai 0, 1, 2, atau 3 dan melakukan aksi sesuai dengan kondisi yang terpenuhi
- `child_id[i] = fork();` : untuk melakukan fork pada proses parent
- `if (child_id[i] < 0)` : untuk melakukan pengecekan apakah variabel `child_id` bernilai kurang dari 0 atau tidak, kurang dari 0 berarti proses tersebut gagal dibuat
- `else if (child_id[i] == 0)` : untuk melakukan pengecekan apakah variabel `child_id` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
- `char *args[] = {"mkdir", "-p", newFolder, NULL};` : untuk mendeklarasikan variabel `args` yang bertipe data char dan memiliki isi array `mkdir`, `-p`, `newFolder`, dan `NULL`
- `execv ("/bin/mkdir", args);` : untuk melakukan eksekusi fungsi `mkdir` dengan argumen `-p` dan `newFolder`
- `while (wait(&stat) > 0)` : untuk melakukan iterasi selama proses child belum selesai
- diakhir while merupakan child_id[i] > 0 yang merupakan proses parent yang menunggu proses child selesai
- if (folder != NULL) : untuk melakukan pengecekan apakah variabel `folder` bernilai NULL atau tidak, NULL berarti proses tersebut gagal dibuat
- `folder = opendir("players");` : untuk membuka folder `players`
- `while ((dp = readdir(folder)) != NULL)` : untuk melakukan iterasi selama proses membaca folder `players` belum selesai
- `if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)` : untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki ekstensi `.png` atau bukan
- `if (dp->d_type == DT_REG && strstr(dp->d_name, newFolder) != NULL)` : untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki ekstensi `.png` dan memiliki nama folder yang sama dengan nama folder yang akan dibuat
- `path = malloc(strlen("players") + strlen(dp->d_name) + 2);` : untuk melakukan alokasi memori pada variabel `path` dengan ukuran memori yang dibutuhkan sesuai dengan panjang string `players` ditambah panjang string `dp->d_name` ditambah 2
- `sprintf(path, "%s/%s", "players", dp->d_name);` : untuk melakukan penulisan pada variabel `path` dengan format string `players/dp->d_name`
- `child_id[i] = fork();` : untuk melakukan fork pada proses parent
- `if (child_id[i] == 0)` : untuk melakukan pengecekan apakah variabel `child_id` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
- `char *args[] = {"mv", "-f", path, newFolder, NULL};` : untuk mendeklarasikan variabel `args` yang bertipe data char dan memiliki isi array `mv`, `-f`, `path`, `newFolder`, dan `NULL`
- `execv ("/bin/mv", args);` : untuk melakukan eksekusi fungsi `mv` dengan argumen `-f`, `path`, dan `newFolder`
- `for (int i = 0; i < 4; i++)` : untuk melakukan iterasi sebanyak 4 kali, iterasi ini digunakan untuk menunggu proses child selesai
- close dir : untuk menutup folder `players`

### Point D
#### Deskripsi
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang,
dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

#### Penyelesaian
untuk menyelesaikan point D, kami menggunakan fungsi `fork()` dan `execlp()`.
`execlp()` digunakan untuk menjalankan program yang ada di path yang sudah ditentukan. sedangkan `fork()` digunakan untuk membuat proses baru.

#### Source Code
```c
void buatTim(int bek, int gelandang, int penyerang){
  int stat;
  pid_t child_id;

  char args[200];
  child_id = fork();

  // Remove file if exist
  if (child_id == 0){
    snprintf(args, sizeof(args), "rm -f ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }
  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Bek | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }
  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Gelandang | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Penyerang | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Kiper | sort -t _ -k4,4 -n -r | head -n 1 >> ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  waitpid(child_id, &stat, 0);
}
```

#### Penjelasan
- `snprintf(args, sizeof(args), "rm -f ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);` : untuk menghapus file Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt jika sudah ada
- `char args[200];` : untuk mendeklarasikan variabel `args` yang bertipe data char dan memiliki ukuran 200
- `if (child_id == 0)` : untuk melakukan pengecekan apakah variabel `child_id` bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child
- `snprintf(args, sizeof(args), "ls ./Bek | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);` : untuk melakukan sorting pada folder `Bek` berdasarkan rating yang terbesar dan mengambil 4 data teratas untuk ditulis pada file `Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt`
- Fungsi diatas diulangi untuk folder `Gelandang`, `Penyerang`, dan `Kiper`
- Selesai  


## Soal 4

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa **Jam** (0-23), **Menit** (0-59), **Detik** (0-59), **Tanda asterisk [ * ]** (value bebas), serta path **file .sh**.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan **pesan “error”** apabila argumen yang diterima program tidak sesuai. **Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.**
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini **berjalan dalam background** dan **hanya menerima satu config cron.**
- Bonus poin apabila CPU state minimum.

**Contoh untuk run**: /program \* 44 5 /home/Banabil/programcron.sh

#### Abstraksi

Inti dari soal nomor 4 ini adalah kami diperintahkan untuk membuatkan banabil sebuah program layaknya crontab yang ada pada sistem namun murni dengan menggunakan bahasa C tanpa bantuan sistem. Bedanya dengan sistem, crontab permintaan banabil hanya perlu menerima 4 parameter saja, yakni **jam**, **menit**, **detik**, dan **filepath** berupa shell script yang akan dieksekusi. nilai untuk parameter waktu, seperti jam, menit, dan detik dapat berupa angka ataupun **[*]**. Banabil juga meminta agar crontabnya dapat menghandle error - error yang mungkin terjadi saat pemanggilan program, seperti nilai parameter jam bukan di antara 0 - 23, jumlah parameter input kurang, dll. Dan yang terakhir, Banabil ingin crontabnya dapat berjalan di background seperti crontab yang ada pada sistem dengan tiap file yang akan dieksekusi hanya menerima 1 konfigurasi cron. Banabil juga akan memberi poin bonus jika CPU state yang kami gunakan bernilai minimal.

#### Penyelesaian

Sebenarnya, persoalan ini dapat langsung diselesaikan menggunakan metode yang dapat dibilang sebagai metode _brute force_, di mana program akan mengecek secara terus menerus setiap detiknya apakah waktu sekarang sudah sama seperti waktu inputan yang diberikan lewat parameter. jika belum dia akan mengecek apakah sudah di detik berikutnya, dan jika sudah, barulah di eksekusi file yang diminta. Namun, menurut kami, hal ini bukanlah sebuah solusi yang efektif dan efisien. Solusi ini juga pasti akan memakan CPU yang lebih besar melihat program terus menerus berjalan untuk mengecek apakah sudah waktunya untuk eksekusi.

Oleh karena itu, kami terpikirkan sebuah solusi yang mana program hanya perlu berjalan pada saat waktu eksekusi telah tiba, selain waktu tersebut, program akan kami sleep agar tidak memakan terlalu banyak CPU. Namun, untuk dapat mengimplementasikan solusi ini, kami perlu mengorbankan minimalitas kode yang membuat kode kami menjadi lebih panjang dibandingkan dengan kode dengan metode sebelumnya. Hal ini disebabkan oleh pengkondisian - pengkondisian yang kami lakukan untuk setiap tipe konfigurasi cron agar program dapat berjalan lebih efisien dan tidak membuang terlalu banyak CPU. Agar lebih jelas, berikut kami tampilkan terlebih dahulu solusi yang kami gunakan untuk penyelesaian persoalan ini,

#### Code

```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>

char filePath[100];

void execvron () {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        char *argvs[3] = {"bash", filePath, NULL};
        execv("/bin/bash", argvs);
    }
}

int main(int argc, char **argv) {
    int hour, min, sec;
    long dif;

    if(argc != 5) {
        printf("argumennya harus 4 mas...\n");
        return 0;
    }

    if(strcmp(argv[1], "*") == 0) {
        hour = -1;
    }
    else {
        hour = atoi(argv[1]);
        if(hour < 0 || hour > 23){
            printf("gaada mas jam segitu...\n");
            return 0;
        }
    }

    if(strcmp(argv[2], "*") == 0) {
    min = -1;
    }
    else {
        min = atoi(argv[2]);
        if(min < 0 || min > 59){
            printf("gaada mas menit segitu...\n");
            return 0;
        }
    }

    if(strcmp(argv[3], "*") == 0) {
        sec = -1;
    }
    else {
        sec = atoi(argv[3]);
        if(sec < 0 || sec > 59){
            printf("gaada mas detik segitu...\n");
            return 0;
        }
    }

    strcpy(filePath, argv[4]);

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
    exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
    exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
    exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    int cursec = timeinfo->tm_sec;
    int curmin = timeinfo->tm_min;
    int curhour = timeinfo->tm_hour;

    // * * *
    if (hour == -1 && min == -1 && sec == -1) {
        while (1) {
            execvron();
            sleep(1);
        }

    // * * n
    } else if (hour == -1 && min == -1 && sec != -1) {
        if(cursec < sec) {
            dif = sec - cursec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(60);
            }
        }
        else if(cursec > sec) {
            dif = 60 - cursec + sec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(60);
            }
        }
        else {
            while(1) {
                execvron();
                sleep(60);
            }
        }

    // * n *
    } else if (hour == -1 && min != -1 && sec == -1) {
        if(curmin > min) {
            dif = (60 - curmin + min)*60 - cursec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(3540);
            }
        }
        else if(curmin < min) {
            dif = (min - curmin)*60 - cursec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(3540);
            }
        }
        else {
            dif = 60 - cursec;
            while(1) {
                int t = 0;
                while(t < dif) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(3540);
                dif = 60;
            }
        }

    // n * *
    } else if (hour != -1 && min == -1 && sec == -1) {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - (curmin*60) - cursec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 3600) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(82800); // 1 hari kurangi 1 jam
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - (curmin*60) - cursec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 3600) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(82800); // 1 hari kurangi 1 jam
            }
        }
        else {
            dif = 3600 - (curmin*60) - cursec;
            while(1) {
                int t = 0;
                while(t < dif) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(82800); // 1 hari kurangi 1 jam
                dif = 3600;
            }
        }

    // * n n
    } else if (hour == -1 && min != -1 && sec != -1) {
        if(curmin > min) {
            dif = (60 - curmin + min)*60 - cursec + sec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(3600);
            }
        }
        else if(curmin < min) {
            dif = (min - curmin)*60 - cursec + sec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(3600);
            }
        }
        else {
            if(cursec < sec) {
                dif = sec - cursec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(3600);
                }
            }
            else if(cursec > sec) {
                dif = 3600 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(3600);
                }
            }
            else {
                while(1) {
                    execvron();
                    sleep(3600);
                }
            }
        }

    // n * n
    } else if (hour != -1 && min == -1 && sec != -1) {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - curmin*60 - cursec + sec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800); // sleep 1 hari kurangi 1 jam
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - curmin*60 - cursec + sec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800); // sleep 1 hari kurangi 1 jam
            }
        }
        else {
            if(cursec < sec) {
                dif = sec - cursec;
                int curdif = 60 - curmin;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < curdif) {
                        execvron();
                        t++;
                        sleep(60);
                    }
                    sleep(82800);
                    curdif = 60;
                }
            }
            else if(cursec > sec) {
                dif = 60 - cursec + sec;
                int curdif = 60 - curmin - 1;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < curdif) {
                        execvron();
                        t++;
                        sleep(60);
                    }
                    sleep(82800);
                    curdif = 60;
                }
            }
            else {
                int curdif = 60 - curmin;
                while(1) {
                    int t = 0;
                    while(t < curdif) {
                        execvron();
                        t++;
                        sleep(60);
                    }
                    sleep(82800);
                    curdif = 60;
                }
            }
        }

    // n n *
    } else if (hour != -1 && min != -1 && sec == -1) {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - (curmin - min)*60 - cursec;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - (curmin - min)*60 - cursec;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
            }
        }
        else {
            if(curmin < min) {
                dif = (min - curmin)*60 - cursec;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < 60) {
                        execvron();
                        t++;
                        sleep(1);
                    }
                    sleep(86340); // sleep 1 hari kurangi 1 menit
                }
            }
            else if(curmin > min) {
                dif = 86400 - (curmin - min)*60 - cursec;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < 60) {
                        execvron();
                        t++;
                        sleep(1);
                    }
                    sleep(86340); // sleep 1 hari kurangi 1 menit
                }
            }
            else {
                dif = 60 - cursec;
                while(1) {
                    int t = 0;
                    while(t < dif) {
                        execvron();
                        t++;
                        sleep(1);
                    }
                    sleep(86340); // sleep 1 hari kurangi 1 menit
                    dif = 60;
                }
            }
        }

    // n n n
    } else {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - (curmin - min)*60 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(86400);
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - (curmin - min)*60 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(86400);
            }
        }
        else {
            if(curmin < min) {
                dif = (min - curmin)*60 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
            else if(curmin > min) {
                dif = 86400 - (curmin - min)*60 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
            else {
                if(cursec < sec) {
                    dif = sec - cursec;
                    sleep(dif);
                    while(1) {
                        execvron();
                        sleep(86400);
                    }
                }
                else if(cursec > sec) {
                    dif = 86400 - cursec + sec;
                    sleep(dif);
                    while(1) {
                        execvron();
                        sleep(86400);
                    }
                }
                else {
                    while(1) {
                        execvron();
                        sleep(86400);
                    }
                }
            }
        }
    }
}
```

#### Penjelasan algoritma

- **Poin pertama**, kami diminta untuk tidak menggunakan sistem. Oleh karena itu, kelompok kami menggunakan fungsi - fungsi bawaan yang ada di dalam library - libray C, seperti sleep, fork, execv, dan lain - lain untuk menjalankan permintaan - permintaan dari Banabil.
- **Poin kedua**, kami diminta untuk dapat menghandle error - error yang mungkin terjadi saat user menjalankan program cron kami. hal ini kami bagi menjadi 2 error handling, yakni saat user menginputkan parameter yang kurang atau lebih dari seharusnya dan saat nilai parameter waktu yang diinputkan tidak sesuai dengan nilai normalnya.
- **Poin ketiga**, kami diminta untuk dapat menerima inputan parameter waktu berupa jam, menit dan detik yang dapat bernilai angka maupun [\*], serta file path yang akan dieksekusi. Hal ini kami lakukan dengan cek terlebih dahulu apakah nilai parameter waktu yang diinputkan berupa bintang [\*] atau tidak. jika iya, maka beri tanda dengan mengisi variabel parameter waktu tersebut dengan **-1** dan jika tidak, maka kami ubah terlebih dahulu tipe datanya menjadi integer akan lebih mudah untuk dioperasikan dalam pengkondisian di program utamanya. Berikut ini adalah potongan kode untuk penyelesaian poin pertama dan point kedua

```c
int hour, min, sec;
    long dif;

    if(argc != 5) {
        printf("argumennya harus 4 mas...\n");
        return 0;
    }

    if(strcmp(argv[1], "*") == 0) {
        hour = -1;
    }
    else {
        hour = atoi(argv[1]);
        if(hour < 0 || hour > 23) {
            printf("gaada mas jam segitu...\n");
            return 0;
        }
    }

    if(strcmp(argv[2], "*") == 0) {
    min = -1;
    }
    else {
        min = atoi(argv[2]);
        if(min < 0 || min > 59) {
            printf("gaada mas menit segitu...\n");
            return 0;
        }
    }

    if(strcmp(argv[3], "*") == 0) {
        sec = -1;
    }
    else {
        sec = atoi(argv[3]);
        if(sec < 0 || sec > 59) {
            printf("gaada mas detik segitu...\n");
            return 0;
        }
    }

    strcpy(filePath, argv[4]);
```

- **Poin keempat**, kami diminta agar program cron dapat berjalan di latar belakang dan hanya dapat menerima 1 konfigurasi cron untuk setiap file yang akan dieksekusi. Hal ini dapat dilakukan dengan menjadikan program cron kami menjadi daemon dan membuat masing - masing konfigurasi cron menjadi child baru agar dapat berjalan secara paralel. Berikut ini adalah potongan kode untuk menyelesaikan poin ini.

```c
pid_t pid, sid;

pid = fork();

if (pid < 0) {
exit(EXIT_FAILURE);
}

if (pid > 0) {
exit(EXIT_SUCCESS);
}

umask(0);

sid = setsid();
if (sid < 0) {
exit(EXIT_FAILURE);
}

if ((chdir("/")) < 0) {
exit(EXIT_FAILURE);
}

close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
```

#### Penjelasan Kode

1. `pid = fork();` : spawn proses baru dan simpan ke variabel pid.
2. `if (pid < 0)` : jika gagal, maka exit dengan EXIT_FAILURE
3. `if (pid > 0)` : jika berhasil, maka bunuh parentnya sehingga sekarang, program adalah childnya
4. `umask(0)` : mengatur nilai umask(0) agar kita mendapatkan akses full terhadap file yang dibuat oleh daemon.
5. `sid = setsid();` : buat unique sid untuk child ini agar bisa tetap berjalan.
6. `if ((chdir("/")) < 0` : ubah working directorynya ke root karena sudah pasti ada.
7. `close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);` : tutup file deskriptor standar untuk memastikan program bekerja di belakang layar

<br><br>

- **Poin terakhir**, adalah poin di mana kelompok kami menyusun algoritma pengkondisian yang akan dilakukan untuk dapat mencapai objektif utama persoalan nomor 4 ini, yakni berapa lama program kami harus di sleep dan kapan saja program kami harus berjalan. Karena parameter waktu yang diminta berjumlah 3, dan inputan dapat berupa angka ataupun bintang, maka program dapat kami bagi menjadi 2<sup>3</sup> = 8 kondisi seperti berikut ini,

| no  |   pola   |       penjelasan        |
| :-: | :------: | :---------------------: |
|  1  | \* \* \* | bintang bintang bintang |
|  2  | \* \* n  |  bintang bintang angka  |
|  3  | \* n \*  |  bintang bintang angka  |
|  4  | n \* \*  |  bintang bintang angka  |
|  5  |  \* n n  |  bintang bintang angka  |
|  6  |  n n \*  |  bintang bintang angka  |
|  7  |  n \* n  |  bintang bintang angka  |
|  8  |  n n n   |  bintang bintang angka  |

sebelum masuk ke dalam pengkondisian masing - masing pola, kita perlu tahu terlebih dahulu waktu sekarang di local masing - masing. Hal ini dapat dilakukan dengan menggunakan salah satu library bawaan C, yakni `time.h`. Setelah itu, barulah kami ambil parameter - parameter waktu yang kami butuhkan, seperti jam, menit, dan detik. Berikut ini adalah potongan kode kami untuk mengambil waktu sekarang.

```c
time_t rawtime;
struct tm * timeinfo;

time ( &rawtime );
timeinfo = localtime ( &rawtime );

int cursec = timeinfo->tm_sec;
int curmin = timeinfo->tm_min;
int curhour = timeinfo->tm_hour;
```

#### Penjelasan Kode

1. `time_t rawtime;` : mendeklarasikan sebuah variabel dengan nama rawtime yang bertipe data time_t.
2. `struct tm * timeinfo;` : mendeklarasikan sebuah pointer dengan nama timeinfo yang menunjuk ke sebuah struct bertipe data tm.
3. `time ( &rawtime );` : memanggil fungsi time() dan memberikan alamat dari rawtime sebagai argumen untuk mendapatkan waktu sistem saat ini.
4. `timeinfo = localtime ( &rawtime );` : memanggil fungsi localtime() dan memberikan alamat dari rawtime sebagai argumen untuk mengonversi waktu sistem mentah menjadi waktu lokal, yang disimpan dalam struct tm yang ditunjuk oleh timeinfo.
5. `int cursec = timeinfo->tm_sec;` : mengambil nilai detik saat ini dari struct tm yang ditunjuk oleh timeinfo dan menyimpannya dalam variabel baru dengan nama cursec.
6. `int curmin = timeinfo->tm_min;` : mengambil nilai menit saat ini dari struct tm yang ditunjuk oleh timeinfo dan menyimpannya dalam variabel baru dengan nama curmin.
7. `int curhour = timeinfo->tm_hour;` : mengambil nilai jam saat ini dari struct tm yang ditunjuk oleh timeinfo dan menyimpannya dalam variabel baru dengan nama curhour.

<br>

Dan untuk eksekusi programnya, kami membuat child untuk setiap eksekusi akan dapat berjalan secara paralel. Berikut ini adalah fungsi dalam program kami untuk mengeksekusi file yang diinputkan melalui parameter,

```c
void execvron () {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argvs[3] = {"bash", filePath, NULL};
        execv("/bin/bash", argvs);
    }
}
```

#### Penjelasan Kode

1. `pid_t child_id;` : variabel bertipe pid_t untuk menyimpan nilai yang dikembalikan dari fungsi `fork()`.
2. `child_id = fork();` : spawn child baru dan nilai nya akan dikembalikan ke variabel child_id.
3. `if (child_id < 0)` : jika proses spawning gagal, maka lakukan blok kode di dalamnya.
4. `if (child_id == 0)` : jika proses spawning berhasil, maka lakukan blok kode di dalamnya.
5. `char *argvs[3] = {"bash", filePath, NULL};` : array untuk menyimpan argumen yang nantinya akan digunakan untuk eksekusi menggunakan execv
6. `execv("/bin/bash", argvs);` : fungsi untuk mengeksekusi argumen yang sebelumnya sudah di simpan di array menggunakan fungsi pada C, yakni execv

<br>

### Pola

1. **Pola pertama**, `\* \* \*`  
   karena ketiga input berupa bintang, maka program akan langsung dan terus dijalankan sejak program dijalankan setiap detiknya. Oleh karena itu, proses eksekusi akan terus berjalan dengan jeda sleep 1 detik. Berikut ini potongan kode untuk programnya,

```c
if (hour == -1 && min == -1 && sec == -1) {
    while (1) {
        execvron();
        sleep(1);
    }
}
```

2. **Pola kedua**, `\* \* n`  
   karena hanya input ketiga yang berupa angka, maka program akan dijalankan di setiap jam, di setiap menit, namun hanya di detik ke _n_ sehingga satu-satunya atribut yang krusial adalah detik. pertama cek terlebih dahulu apakah detik sekarang sudah melewati detik inputan atau belum. Jika belum, maka program kami sleep selama selisih detik tersebut lalu bangunkan dan eksekusi program sekali. Karena program akan melakukan eksekusi lagi di menit berikutnya di detik ke _n_ lagi, maka kami sleep terlebih dahulu program selama 60 detik. berikut ini potongan kode untuk programnya,

```c
else if (hour == -1 && min == -1 && sec != -1) {
    if(cursec < sec) {
        dif = sec - cursec;

        sleep(dif);
        while(1) {
            execvron();
            sleep(60);
        }
    }
    else if(cursec > sec) {
        dif = 60 - cursec + sec;

        sleep(dif);
        while(1) {
            execvron();
            sleep(60);
        }
    }
    else {
        while(1) {
            execvron();
            sleep(60);
        }
    }
}
```

3. **Pola ketiga**, `\* n \*`  
   karena hanya input kedua yang berupa angka, maka program akan dijalankan di setiap jam, di setiap detik, namun hanya di menit ke _n_ sehingga satu-satunya atribut yang krusial adalah menit. Konsep pengerjaannya kurang lebih sama seperti pola kedua, namun bedanya disini, yang kita cek adalah menitnya. Jika belum sampai, maka cari selisihnya dan sleep selama selisih tersebut, jika melebihi, maka cari berapa lama agar waktu sekarang bisa mencapai menit ke _n_ berikutnya, baru sleep selama selang waktu tersebut. jika sudah sama, maka dapat langsung dieksekusi. kemudian barulah kita sleep selama 1 jam kurang 1 menit untuk eksekusi berikutnya. Berikut ini adalah potongan kode kami untuk pola ketiga,

```c
else if (hour == -1 && min != -1 && sec == -1) {
    if(curmin > min) {
        dif = (60 - curmin + min)*60 - cursec;

        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 60) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(3540);
        }
    }
    else if(curmin < min) {
        dif = (min - curmin)*60 - cursec;

        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 60) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(3540);
        }
    }
    else {
        dif = 60 - cursec;
        while(1) {
            execvron();
            int t = 0;
            while(t < dif) {
                sleep(1);
                t++;
                execvron();
            }
            sleep(3540);
            dif = 60;
        }
    }
}
```

4. **Pola keempat**, `n \* \*`  
   karena hanya input pertama yang berupa angka, maka program akan dijalankan di setiap menit, di setiap detik, namun hanya di jam ke _n_ sehingga satu-satunya atribut yang krusial adalah jam. Konsep pengerjaannya kurang lebih sama seperti pola sebelumnya, namun bedanya disini, yang kita cek adalah jamnya. Jika belum sampai, maka cari selisihnya dan sleep selama selisih tersebut, jika melebihi, maka cari berapa lama agar waktu sekarang bisa mencapai jam ke _n_ berikutnya, baru sleep selama selang waktu tersebut. Kemudian program akan mengeksekusi setiap detiknya selama 1 jam, lalu barulah kami sleep lagi selama 1 hari kurang 1 jam untuk eksekusi berikutnya. Namun jika sudah sama, maka dapat langsung dieksekusi dan sleep lagi selama 1 hari kurang 1 jam. Berikut ini adalah potongan kode kami untuk pola keempat,

```c
else if (hour != -1 && min == -1 && sec == -1) {
    if(curhour < hour) {
        dif = (hour - curhour)*3600 - (curmin*60) - cursec;

        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 3600) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(82800); // 1 hari kurangi 1 jam
        }
    }
    else if(curhour > hour) {
        dif = (24 - curhour + hour)*3600 - (curmin*60) - cursec;

        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 3600) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(82800); // 1 hari kurangi 1 jam
        }
    }
    else {
        dif = 3600 - (curmin*60) - cursec;
        while(1) {
            int t = 0;
            while(t < dif) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(82800); // 1 hari kurangi 1 jam
            dif = 3600;
        }
    }
}
```

5. **Pola kelima**, `\* n n`  
   karena hanya input pertama yang berupa bintang, maka program akan dijalankan di setiap jam, namun hanya di menit dan di detik tertentu. sehingga atribut yang krusial untuk pola ini ada 2, yakni menit dan detik. Oleh karena itu, parameter yang kita cek pertama adalah menit, kemudian baru kita cek detik. pengecekan kondisi yang dilakukan juga hampir sama, yaitu kondisi jika menit sekarang belum sampai, jika sudah lewat, dan jika sudah sampai. Namun jika sudah sampai, kita perlu cek lagi kondisi untuk detiknya, yaitu saat detik belum sampai, saat detik sudah terlewat, dan saat detik sudah sampai. Proses pengecekan dan operasi eksekusi juga kurang lebih sama seperti sebelumnya. Yang berbeda hanyalah waktu untuk sleep si program. Karena program hanya akan mengeksekusi di menit dan detik tertentu di setiap jam, maka eksekusi akan dilakukan sekali setiap jam, kemudian sleep program selama 1 jam untuk eksekusi berikutnya. Berikut ini adalah potongan kode kami untuk pola kelima,

```c
else if (hour == -1 && min != -1 && sec != -1) {
    if(curmin > min) {
        dif = (60 - curmin + min)*60 - cursec + sec;

        sleep(dif);
        while(1) {
            execvron();
            sleep(3600);
        }
    }
    else if(curmin < min) {
        dif = (min - curmin)*60 - cursec + sec;

        sleep(dif);
        while(1) {
            execvron();
            sleep(3600);
        }
    }
    else {
        if(cursec < sec) {
            dif = sec - cursec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(3600);
            }
        }
        else if(cursec > sec) {
            dif = 3600 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(3600);
            }
        }
        else {
            while(1) {
                execvron();
                sleep(3600);
            }
        }
    }
}
```

6. **Pola keenam**, `n n \*`  
   karena hanya input ketiga yang berupa bintang, maka program akan dijalankan di setiap detik, namun hanya di jam dan di menit tertentu. sehingga atribut yang krusial untuk pola ini ada 2, yakni jam dan menit. Oleh karena itu, parameter yang kita cek pertama adalah jam, kemudian baru kita cek menit. pengecekan kondisi yang dilakukan juga hampir sama, yaitu kondisi jika jam sekarang belum sampai, jika sudah lewat, dan jika sudah sampai. Namun jika sudah sampai, kita perlu cek lagi kondisi untuk menitnya, yaitu saat menit belum sampai, saat menit sudah terlewat, dan saat menit sudah sampai. Proses pengecekan dan operasi eksekusi juga kurang lebih sama seperti sebelumnya. Yang berbeda hanyalah waktu untuk sleep si program. Karena program hanya akan mengeksekusi di jam dan menit tertentu di setiap detik, maka eksekusi akan dilakukan sekali setiap hari selama 1 menit, kemudian sleep program selama 1 hari kurang 1 menit untuk eksekusi berikutnya. Berikut ini adalah potongan kode kami untuk pola keenam,

```c
else if (hour != -1 && min != -1 && sec == -1) {
    if(curhour < hour) {
        dif = (hour - curhour)*3600 - (curmin - min)*60 - cursec;
        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 60) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(86340); // sleep 1 hari kurangi 1 menit
        }
    }
    else if(curhour > hour) {
        dif = (24 - curhour + hour)*3600 - (curmin - min)*60 - cursec;
        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 60) {
                execvron();
                t++;
                sleep(1);
            }
            sleep(86340); // sleep 1 hari kurangi 1 menit
        }
    }
    else {
        if(curmin < min) {
            dif = (min - curmin)*60 - cursec;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
            }
        }
        else if(curmin > min) {
            dif = 86400 - (curmin - min)*60 - cursec;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
            }
        }
        else {
            dif = 60 - cursec;
            while(1) {
                int t = 0;
                while(t < dif) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
                dif = 60;
            }
        }
    }
}
```

7. **Pola ketujuh**, `n \* n`  
   karena hanya input kedua yang berupa bintang, maka program akan dijalankan di setiap menit, namun hanya di jam dan di detik tertentu. sehingga atribut yang krusial untuk pola ini ada 2, yakni jam dan detik. Oleh karena itu, parameter yang kita cek pertama adalah jam, kemudian baru kita cek menit. pengecekan kondisi yang dilakukan juga hampir sama, yaitu kondisi jika jam sekarang belum sampai, jika sudah lewat, dan jika sudah sampai. Namun jika sudah sampai, kita perlu cek lagi kondisi untuk menitnya, yaitu saat menit belum sampai, saat menit sudah terlewat, dan saat menit sudah sampai. Proses pengecekan dan operasi eksekusi juga kurang lebih sama seperti sebelumnya. Yang berbeda hanyalah waktu untuk sleep si program. Karena program hanya akan mengeksekusi di jam dan detik tertentu di setiap menit, maka eksekusi akan dilakukan 60 kali setiap hari, kemudian sleep program selama 1 hari kurang 1 jam untuk eksekusi berikutnya. Berikut ini adalah potongan kode kami untuk pola ketujuh,

```c
else if (hour != -1 && min == -1 && sec != -1) {
    if(curhour < hour) {
        dif = (hour - curhour)*3600 - curmin*60 - cursec + sec;

        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 60) {
                execvron();
                t++;
                sleep(60);
            }
            sleep(82800); // sleep 1 hari kurangi 1 jam
        }
    }
    else if(curhour > hour) {
        dif = (24 - curhour + hour)*3600 - curmin*60 - cursec + sec;

        sleep(dif);
        while(1) {
            int t = 0;
            while(t < 60) {
                execvron();
                t++;
                sleep(60);
            }
            sleep(82800); // sleep 1 hari kurangi 1 jam
        }
    }
    else {
        if(cursec < sec) {
            dif = sec - cursec;
            int curdif = 60 - curmin;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < curdif) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800);
                curdif = 60;
            }
        }
        else if(cursec > sec) {
            dif = 60 - cursec + sec;
            int curdif = 60 - curmin - 1;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < curdif) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800);
                curdif = 60;
            }
        }
        else {
            int curdif = 60 - curmin;
            while(1) {
                int t = 0;
                while(t < curdif) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800);
                curdif = 60;
            }
        }
    }
}
```

8. **Pola kedelapan**, `n n n`  
   karena ketiga input adalah angk, maka semua atribut waktu di pola ini bersifat krusial sehingga pengecekkan dilakukan untuk setiap atribut. Urutan pengecekkan juga sama, parameter yang kita cek pertama adalah jam, kemudian baru kita cek menit, kemudian baru kita cek detik. pengecekan kondisi yang dilakukan juga hampir sama, yaitu kondisi jika jam sekarang belum sampai, jika sudah lewat, dan jika sudah sampai. Namun jika sudah sampai, kita perlu cek lagi kondisi untuk menitnya, yaitu saat menit belum sampai, saat menit sudah terlewat, dan saat menit sudah sampai. Namun jika sudah sampai juga, kita perlu cek lagi kondisi untuk detiknya, yaitu saat detik belum sampai, saat detik sudah terlewat, dan saat detik sudah sampai. Proses pengecekan dan operasi eksekusi juga kurang lebih sama seperti sebelumnya. Yang berbeda hanyalah waktu untuk sleep si program. Karena program hanya akan mengeksekusi di jam, menit, dan detik tertentu setiap harinya, maka eksekusi akan dilakukan sekali setiap hari, kemudian sleep program selama 1 hari untuk eksekusi berikutnya. Berikut ini adalah potongan kode kami untuk pola kedelapan,

```c
else {
    if(curhour < hour) {
        dif = (hour - curhour)*3600 - (curmin - min)*60 - cursec + sec;
        sleep(dif);
        while(1) {
            execvron();
            sleep(86400);
        }
    }
    else if(curhour > hour) {
        dif = (24 - curhour + hour)*3600 - (curmin - min)*60 - cursec + sec;
        sleep(dif);
        while(1) {
            execvron();
            sleep(86400);
        }
    }
    else {
        if(curmin < min) {
            dif = (min - curmin)*60 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(86400);
            }
        }
        else if(curmin > min) {
            dif = 86400 - (curmin - min)*60 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(86400);
            }
        }
        else {
            if(cursec < sec) {
                dif = sec - cursec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
            else if(cursec > sec) {
                dif = 86400 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
            else {
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
        }
    }
}
```

#### Penjelasan Kode

1. `hour` : variabel untuk menyimpan jam inputan.
2. `min` : variabel untuk menyimpan menit inputan.
3. `sec` : variabel untuk menyimpan detik inputan.
4. `curhour` : variabel untuk menyimpan jam sekarang.
5. `curmin` : variabel untuk menyimpan menit sekarang
6. `cursec` : variabel untuk menyimpan detik sekarang.
7. `dif` : variabel untuk menyimpan total selisih waktu sekarang dengan waktu inputan yang nantinya akan digunakan untuk waktu sleep program sebelum eksekusi pertama kali.
8. `curdif` : variabel untuk menyimpan selisih tambahan jika ada
9. `sleep` : fungsi bawaan dari C untuk menidurkan program jika sedang tidak mengeksekusi.
10. `execvron()` : fungsi buatan untuk mengeksekusi file inputan  

#### Contoh Input
```
./mainan \* \* \* /home/vron/programcron.sh
```

#### Contoh Program
```
echo "yee berhasil mas" >> /home/vron/sisop/praktikum-2/hasilcron.txt
```  

#### Contoh Output
```
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
yee berhasil mas
```
