#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>

char filePath[100];

void execvron () {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        char *argvs[3] = {"bash", filePath, NULL};
        execv("/bin/bash", argvs);
    }
}

int main(int argc, char **argv) {
    int hour, min, sec;
    long dif;

    if(argc != 5) {
        printf("argumennya harus 4 mas...\n");
        return 0;
    }

    if(strcmp(argv[1], "*") == 0) {
        hour = -1;
    }
    else {
        hour = atoi(argv[1]);
        if(hour < 0 || hour > 23){
            printf("gaada mas jam segitu...\n");
            return 0;
        }
    }

    if(strcmp(argv[2], "*") == 0) {
    min = -1;
    }
    else {
        min = atoi(argv[2]);
        if(min < 0 || min > 59){
            printf("gaada mas menit segitu...\n");
            return 0;
        }
    }

    if(strcmp(argv[3], "*") == 0) {
        sec = -1;
    }
    else {
        sec = atoi(argv[3]);
        if(sec < 0 || sec > 59){
            printf("gaada mas detik segitu...\n");
            return 0;
        }
    }

    strcpy(filePath, argv[4]);

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
    exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
    exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
    exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    int cursec = timeinfo->tm_sec;
    int curmin = timeinfo->tm_min;
    int curhour = timeinfo->tm_hour;

    // * * *
    if (hour == -1 && min == -1 && sec == -1) {
        while (1) {
            execvron();
            sleep(1);
        }

    // * * n
    } else if (hour == -1 && min == -1 && sec != -1) {
        if(cursec < sec) {
            dif = sec - cursec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(60);
            }
        }
        else if(cursec > sec) {
            dif = 60 - cursec + sec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(60);
            }
        }
        else {
            while(1) {
                execvron();
                sleep(60);
            }
        }

    // * n *
    } else if (hour == -1 && min != -1 && sec == -1) {
        if(curmin > min) {
            dif = (60 - curmin + min)*60 - cursec;
            
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(3540);
            }
        }
        else if(curmin < min) {
            dif = (min - curmin)*60 - cursec;
            
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(3540);
            }
        }
        else {
            dif = 60 - cursec;
            while(1) {
                int t = 0;
                while(t < dif) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(3540);
                dif = 60;
            }
        }

    // n * *
    } else if (hour != -1 && min == -1 && sec == -1) {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - (curmin*60) - cursec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 3600) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(82800); // 1 hari kurangi 1 jam
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - (curmin*60) - cursec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 3600) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(82800); // 1 hari kurangi 1 jam
            }
        }
        else {
            dif = 3600 - (curmin*60) - cursec;
            while(1) {
                int t = 0;
                while(t < dif) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(82800); // 1 hari kurangi 1 jam
                dif = 3600;
            }
        }

    // * n n
    } else if (hour == -1 && min != -1 && sec != -1) {
        if(curmin > min) {
            dif = (60 - curmin + min)*60 - cursec + sec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(3600);
            }
        }
        else if(curmin < min) {
            dif = (min - curmin)*60 - cursec + sec;

            sleep(dif);
            while(1) {
                execvron();
                sleep(3600);
            }
        }
        else {
            if(cursec < sec) {
                dif = sec - cursec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(3600);
                }
            }
            else if(cursec > sec) {
                dif = 3600 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(3600);
                }
            }
            else {
                while(1) {
                    execvron();
                    sleep(3600);
                }
            }
        }

    // n * n
    } else if (hour != -1 && min == -1 && sec != -1) {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - curmin*60 - cursec + sec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800); // sleep 1 hari kurangi 1 jam
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - curmin*60 - cursec + sec;

            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(60);
                }
                sleep(82800); // sleep 1 hari kurangi 1 jam
            }
        }
        else {
            if(cursec < sec) {
                dif = sec - cursec;
                int curdif = 60 - curmin;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < curdif) {
                        execvron();
                        t++;
                        sleep(60);
                    }
                    sleep(82800);
                    curdif = 60;
                }
            }
            else if(cursec > sec) {
                dif = 60 - cursec + sec;
                int curdif = 60 - curmin - 1;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < curdif) {
                        execvron();
                        t++;
                        sleep(60);
                    }
                    sleep(82800);
                    curdif = 60;
                }
            }
            else {
                int curdif = 60 - curmin;
                while(1) {
                    int t = 0;
                    while(t < curdif) {
                        execvron();
                        t++;
                        sleep(60);
                    }
                    sleep(82800);
                    curdif = 60;
                }
            }
        }

    // n n *
    } else if (hour != -1 && min != -1 && sec == -1) {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - (curmin - min)*60 - cursec;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - (curmin - min)*60 - cursec;
            sleep(dif);
            while(1) {
                int t = 0;
                while(t < 60) {
                    execvron();
                    t++;
                    sleep(1);
                }
                sleep(86340); // sleep 1 hari kurangi 1 menit
            }
        }
        else {
            if(curmin < min) {
                dif = (min - curmin)*60 - cursec;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < 60) {
                        execvron();
                        t++;
                        sleep(1);
                    }
                    sleep(86340); // sleep 1 hari kurangi 1 menit
                }
            }
            else if(curmin > min) {
                dif = 86400 - (curmin - min)*60 - cursec;
                sleep(dif);
                while(1) {
                    int t = 0;
                    while(t < 60) {
                        execvron();
                        t++;
                        sleep(1);
                    }
                    sleep(86340); // sleep 1 hari kurangi 1 menit
                }
            }
            else {
                dif = 60 - cursec;
                while(1) {
                    int t = 0;
                    while(t < dif) {
                        execvron();
                        t++;
                        sleep(1);
                    }
                    sleep(86340); // sleep 1 hari kurangi 1 menit
                    dif = 60;
                }
            }
        }

    // n n n
    } else {
        if(curhour < hour) {
            dif = (hour - curhour)*3600 - (curmin - min)*60 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(86400);
            }
        }
        else if(curhour > hour) {
            dif = (24 - curhour + hour)*3600 - (curmin - min)*60 - cursec + sec;
            sleep(dif);
            while(1) {
                execvron();
                sleep(86400);
            }
        }
        else {
            if(curmin < min) {
                dif = (min - curmin)*60 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
            else if(curmin > min) {
                dif = 86400 - (curmin - min)*60 - cursec + sec;
                sleep(dif);
                while(1) {
                    execvron();
                    sleep(86400);
                }
            }
            else {
                if(cursec < sec) {
                    dif = sec - cursec;
                    sleep(dif);
                    while(1) {
                        execvron();
                        sleep(86400);
                    }
                }
                else if(cursec > sec) {
                    dif = 86400 - cursec + sec;
                    sleep(dif);
                    while(1) {
                        execvron();
                        sleep(86400);
                    }
                }
                else {
                    while(1) {
                        execvron();
                        sleep(86400);
                    }
                }
            }
        }
    }
}