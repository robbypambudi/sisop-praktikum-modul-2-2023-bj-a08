#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#define ull unsigned long long

void killer(pid_t pid, char param);
void ngezip(char *path, char *usrtime);

int main(int argc, char *argv[]){
    //memastikan inputan saat menjalankan berargumen -a atau -b
    if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Argumen hanya boleh -a atau -b");
        return 0;
    }
    char dir[100];
    //mendapatkan directory kita saat ini
    getcwd(dir,sizeof(dir));

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

      /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    //memanggil fungsil killer
    killer(getpid(), argv[1][1]);
    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    killer(getpid(), argv[1][1]);
    while (1) {
        //membuat parent proccess
        pid_t parent;
        time_t waktu;
        char usrtime[100];
        char path[100];
        struct tm *info;
        time(&waktu);
        info = localtime(&waktu);

        strftime(usrtime, 100, "%Y-%m-%d_%X", info);

        sprintf(path, "%s/%s", dir, usrtime);
        //forking parent
        parent = fork();
        if(parent < 0)
            exit(EXIT_FAILURE);
        if(parent == 0){
            pid_t donlod;
            int status_donlod;
            donlod = fork();
            if(donlod < 0)
                exit(EXIT_FAILURE);
            if(donlod == 0){
                pid_t folder;
                folder = fork();
                if(folder < 0)
                    exit(EXIT_FAILURE);
                //child proccess untuk melakukan mkdir
                if(folder == 0){
                    char *argfolder[] = {"mkdir", "-p", path, NULL};
                    execv("/bin/mkdir", argfolder);
                }
                //proccess dari parent donlod untuk melakukan download gambar
                ull waktu = (ull)time(NULL);
                waktu = (waktu%1000) + 50;
                char url[100];
                
                sprintf(url, "https://picsum.photos/%llu", waktu);
                
                time_t time_download;
                struct tm *downloadt;
                char name_time[100];
                char dir_download[100];

                for (int i = 0; i < 15; i++){
                    time(&time_download);
                    downloadt = localtime(&time_download);

                    strftime(name_time, 100, "%Y-%m-%d_%X", downloadt);
                    sprintf(dir_download, "%s/%s", usrtime, name_time);
                    pid_t gambar = fork();
                    if(gambar < 0)
                        exit(EXIT_FAILURE);
                    if(gambar == 0){
                        //proccess dari child gambar untuk melakukan download gambar
                        char *argv[] = {"wget", "-q", "-O", dir_download, url, NULL};
                        execv("/usr/bin/wget", argv);
                    }
                    sleep(5);
                }
            }
            wait(&status_donlod);
            ngezip(usrtime, usrtime);
        }
        sleep(30);
    }

}


//fungsi untuk melakukan zip terhadap folder yang telah dibuat lalu menghapus folder tersebut
void ngezip(char *path, char *usrtime){
    char zip_name[100];
    sprintf(zip_name, "%s.zip", usrtime);
    char *argv[] = {"zip", "-rm", zip_name, path, NULL};
    execv("/usr/bin/zip", argv);
}
//fungsi untuk mengenerate killer
void killer(pid_t pid, char param){
    char cwd[100];
    //mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;
    int test;
    //melakukan forking pada parent
    pid_t parent = fork();
    if (parent < 0) 
        exit(EXIT_FAILURE);

    if (parent == 0) {
        FILE *fp;
        char killer_file[100];
        char data[1000];
        char dest[100];
        char file_name[100];
        char mode[100];
        
        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);

        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a') 
          strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b') 
          sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status)>0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    while(wait(&status) > 0);
    //melakukan forking pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0) 
      exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[100];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }
    
    while(wait(&status) > 0);
}